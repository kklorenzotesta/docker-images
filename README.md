# Docker images

This repository contains docker images for the following projects:

- [/youtubepreamp](youtubepreamp/Dockerfile) image for the CI of [YoutubePreamp](https://gitlab.com/kklorenzotesta/youtubepreamp), based on alpine contains openjdk 8, git and sbt 1.3.8 with already loaded scala 2.12.11, 2.13.0 and 2.13.1
- [/homebuttonwheel](homebuttonwheel/Dockerfile) image for the CI of [Home Button Wheel](https://gitlab.com/kklorenzotesta/home-button-wheel), based on alpine contains openjdk 8 and all the tools needed to compile android on sdk 29
